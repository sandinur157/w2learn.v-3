<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Request;
use Session;

class PostController extends Controller
{

    public function index()
    {
        $posts      = Post::latest()->approved()->published()->paginate(9);
        $latests    = Post::latest()->approved()->published()->take(8)->get();

        return view('new-article', compact('posts', 'latests'));
    }
    
    public function detail($slug)
    {
        $post = Post::where('slug', $slug)->approved()->published()->first();
        $prev = Post::where('id', '<', "$post->id")->latest()->first();
        $next = Post::where('id', '>', "$post->id")->first();

        $blogKey = 'blog_' . $post->id;

        if (!Session::has($blogKey)) {
            $post->increment('view_count');
            Session::put($blogKey, 1);
        }

        return view('new-single', compact('post', 'prev', 'next'));
    }

    public function postByCategory($slug)
    {
        $category = Category::where('slug', $slug)->first();
        $posts    = !empty($category) ? $category->posts()->approved()->published()->paginate(9) : '';

        return view('new-category', compact('category', 'posts'));
    }

    public function postByTag($slug)
    {
        $tag = Tag::where('slug', $slug)->first();
        $posts = !empty($tag) ? $tag->posts()->approved()->published()->paginate(9) : '';

        return view('new-tag', compact('tag', 'posts'));
    }

    public function toc()
    {
        $tocs = Post::latest()->select('title', 'slug')->get();
        $categories = Category::inRandomOrder()->get();

        return view('new-daftar-isi', compact('tocs', 'categories'));
    }
}
