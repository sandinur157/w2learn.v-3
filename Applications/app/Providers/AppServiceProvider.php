<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use Blade;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Blade::component('components.card', 'card');
        Blade::component('components.table', 'table');
        
        View::composer('layouts.frontend.partials.widget', function ($view) {
            $categories = \App\Category::all();
            $view->with('categories', $categories);

            $tags = \App\Tag::all();
            $view->with('tags', $tags);

            $posts = \App\Post::approved()->published()->orderBy('view_count', 'desc')->take(5)->get();
            $view->with('popular_posts', $posts);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
