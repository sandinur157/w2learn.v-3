@extends('layouts.frontend.master')

@section('title', 'Search')

@push('css')
    <style>
        .card-body h1:not(.blog-title),
        .card-body h2,
        .card-body h3:not(.header-title),
        .card-body h4,
        .card-body h5:not(.card-title),
        .card-body h6:not(.author)
        {
            font-size: 16px;
        }
    </style>
@endpush

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class="font-weight-light">{{ $posts_count }} Hasil Pencarian Untuk :</h3>
            <h1 class="text-capitalize">#{{ $query }}</h1>
        </div>
    </div>
</div>
@endsection

@section('main-content')
<div class="container-fluid">
    <div class="row pt-5">
        <div class="col-md-9">
            @if ($posts->count() == 0)
                <div class="card" style="box-shadow: none; background-color: transparent;">
                    <img src="{{ asset('/img/result-not-found.png') }}" alt="" width="400" style="margin: auto;">
                </div>
            @endif

            <!-- Ads -->
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                 data-ad-client="ca-pub-9070824081326686"
                 data-ad-slot="5334813540"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

            @foreach($posts as $post)
                <div class="row justify-content-center">
                    <div class="col-md-11 card">
                        <div class="row">
                            <div class="col-md-4 col-4 p-2">
                                <div class="over-image-blog rounded">
                                    <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" class="img-fluid" alt="..." style="margin: auto;">
                                </div>
                            </div>
                            <div class="col-md-8 col-8">
                                <div class="card-body">
                                    <div class="card-tools float-right">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-share-alt"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="margin-left: -100px; margin-top: -5px;">
                                                <a href="#" class="dropdown-item"><i class="fab fa-twitter"></i> Twitter</a>
                                                <a href="#" class="dropdown-item"><i class="fab fa-facebook"></i> Facebook</a>
                                                <a href="#" class="dropdown-item"><i class="fab fa-instagram"></i> Instagram</a>
                                                <a href="#" class="dropdown-item"><i class="fab fa-google-plus"></i> Google +</a>
                                                <a href="#" class="dropdown-item"><i class="fab fa-linkedin"></i> Linked in</a>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach ($post->categories as $category)
                                        <span class="tags d-none d-lg-inline-block gradient-indigo">{{ $category->name }}</span>
                                    @endforeach
                                    <a href="{{ url('/post/'. $post->slug) }}"><h1 class="font-weight-bold blog-title text-dark">{{ $post->title }}</h1></a>
                                    <h5 class="font-weight-light mt-3 blog-body">
                                        {!! str_limit($post->body, 100) !!}
                                    </h5>
                                    
                                    <p class="card-title mt-3 font-weight-light blog-tools">
                                        <span style="font-size: .8em"><i class="fas fa-clock mr-2"></i> Reading times</span>
                                        <span class="pb-1 px-1 mx-2">
                                            <small class="text-muted text-sm">&laquo; {{ estimate_reading_time($post->body) }}</small>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <!-- Ads -->
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                 data-ad-client="ca-pub-9070824081326686"
                 data-ad-slot="5334813540"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

            <div class="row">
                <div class="col-12">
                    <nav aria-label="Page navigation example">
                        {{ $posts->links('vendor.pagination.default') }}
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-3" id="widget-col3">
            @include('layouts.frontend.partials.widget')
        </div>
    </div>
</div>
@endsection

@push('scripts')
	<script>
		$('.content-wrapper').css('marginTop', '0')
	</script>
@endpush