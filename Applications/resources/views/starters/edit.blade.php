@extends('layouts.master')

@section('title', 'Edit Produk')

@section('breadcrumb')
	@parent
	<li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
	<li class="breadcrumb-item active">Edit</li>
@endsection

@section('main-content')
<div class="row">
	<div class="col-md-6">
		@card
			@slot('title')
                <h3 class="card-title">Edit Produk</h3>
            @endslot

			<form action="{{ route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
				@csrf @method('put')

                <div class="form-group">
                    <label for="code">Kode</label>
                    <input type="text" name="code" id="code" class="form-control" required readonly value="{{ $product->code }}">
                </div>
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" class="form-control" required value="{{ $product->name }}">
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="description" rows="3" class="form-control">{{ $product->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="stock">Stok</label>
                    <input type="number" name="stock" id="stock" class="form-control" required value="{{ $product->stock }}">
                </div>
                <div class="form-group">
                    <label for="price">Harga</label>
                    <input type="number" name="price" id="price" class="form-control" required value="{{ $product->price }}">
                </div>
                <div class="form-group">
                    <label for="category_id">Kategori</label>
                    <select name="category_id" id="category_id" class="form-control" required>
                        <option>-- Pilih Kategori --</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{ $category->id == $product->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="#">Foto</label>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <img src="{{ Storage::disk('public')->url('uploads/products/'. $product->photo) }}" alt="{{ $product->photo }}" width="100%" class="rounded border">
                        </div>
                        <div class="col-md-8">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="photo" name="photo">
                                <label class="custom-file-label" for="photo">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-sm">Simpan</button>
                </div>
			</form>

			@include('components.errors')
		@endcard
	</div>
</div>
@endsection