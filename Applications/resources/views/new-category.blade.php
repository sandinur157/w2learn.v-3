@extends('layouts.frontend.master')

@section('title', 'Categories')
@push('css')
    <style>
        .card-body h1,
        .card-body h2,
        .card-body h3:not(.header-title),
        .card-body h4,
        .card-body h5:not(.card-title),
        .card-body h6:not(.author)
        {
            font-size: 16px;
        }
    </style>
@endpush

@section('banner')
<div class="container-fluid single-intro">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="header-title">
                {{ $category->name }}
            </h1>
            <p class="card-title header-title">~ Is programming language very simple and very ease.</p>
        </div>
    </div>
</div>
@endsection

@section('main-content')
<div class="container-fluid">
    <div class="row pt-5">
        <div class="col-md-9">
            @if ($posts->count() == 0)
                <div class="card" style="box-shadow: none; background-color: transparent;">
                    <img src="{{ asset('/img/result-not-found.png') }}" alt="" width="400" style="margin: auto;">
                </div>
            @endif
            <div class="row">
                <!-- Ads -->
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="fluid"
                     data-ad-layout-key="-5m+d1+5s-mf+hy"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="5334813540"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                @foreach ($posts as $post)
                    <div class="col-lg-4">
                        <div class="card m-1">
                            <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <small class="text-muted">
                                    <span class="date mt-1">{{ $post->created_at->toFormattedDateString() }}</span>
                                    <span class="float-right" style="margin-top: 4px;">
                                        @foreach($post->tags as $tag)
                                            <a class="text-indigo text-uppercase" href="{{ route('tag.posts', $tag->slug) }}">{{ $tag->name }}</a>,
                                        @endforeach
                                    </span>
                                </small>
                                <a href="{{ route('post.detail', $post->slug) }}" class="text-dark">
                                    <h5 class="card-title my-3">{{ $post->title }}</h5>
                                </a>
                                <p class="card-text card-content">
                                    {!! str_limit($post->body, 150) !!}
                                </p>

                                <div class="media">
                                    <img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}" class="mr-3 rounded" alt="..." width="50">
                                    <div class="media-body">
                                        <h6 class="mt-0 ml-1 author">{{ $post->user->name }}</h6>
                                        <small class="d-inline-block text-muted pr-1"><i class="fas fa-clock"></i> {{ $post->created_at->diffForHumans() }}</small>
                                        <small class="d-inline-block text-muted border-left pl-2"><i class="fas fa-eye"></i> {{ $post->view_count }}</small> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <!-- Ads -->
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="fluid"
                     data-ad-layout-key="-5m+d1+5s-mf+hy"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="5334813540"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            <div class="row">
                <div class="col-12">
                    <nav aria-label="Page navigation example">
                        {{ $posts->links('vendor.pagination.default') }}
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('layouts.frontend.partials.widget')
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('.content-wrapper').css('marginTop', '0')
    </script>
@endpush