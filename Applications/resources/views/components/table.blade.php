<table class="table table-sm table-striped table-bordered table-responsive-sm">
    <thead>
        {{ $thead }}
    </thead>
    <tbody>
        {{ $slot }}
    </tbody>
</table>