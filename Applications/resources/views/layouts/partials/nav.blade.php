<nav class="main-header navbar navbar-expand bg-indigo navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link d-none d-lg-block" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>

            <a href="#" class="nav-link d-lg-none d-block"  data-widget="pushmenu" style="margin-left: -1em;">
                <img src="{{ asset('/img/icon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
                <span class="brand-text font-weight-light" style="font-size: 1.5em">W2Learn CMS</span>
            </a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <a href="{{ route('home') }}" class="nav-item nav-link" title="Go To Website."><i class="fas fa-reply-all"></i></a>
        <a href="{{ route('logout') }}" class="nav-item nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
        <form action="{{ route('logout') }}" method="post" class="d-none d-lg-none" id="logout-form">
            @csrf
        </form>
    </ul>
</nav>