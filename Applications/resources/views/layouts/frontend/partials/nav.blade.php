<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-indigo shadow-sm border-bottom-0" style="background-color: #4a01c1;">
    <div class="container">
        <a href="{{ url('/') }}" class="navbar-brand">
            <img src="{{ asset('/img/logo2.png') }}" alt="W2Learn Logo" class="brand-image elevation-0">
        </a>
        <!-- SEARCH FORM -->
        <form class="form-inline ml-3" action="{{ route('search') }}">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" id="navbar-search" name="query" value="{{ !empty('query') ? request('query') : '' }}">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>

            <div class="card" id="card-search">
                <div class="card-body p-0">
                    <ul id="search-content"></ul>
                </div>
            </div>
        </form>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item d-none d-lg-block d-md-block">
                <a href="{{ url('/articles?page=2') }}" class="nav-link">Artikel</a>
            </li>
            <li class="nav-item d-none d-lg-block d-md-block">
                <a href="{{ url('/daftar-isi') }}" class="nav-link">Daftar Isi</a>
            </li>
            <li class="nav-item d-none d-lg-block d-md-block">
                <a href="{{ url('/templates') }}" class="nav-link">Template</a>
            </li>
            <li class="nav-item d-none d-lg-block dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    Informasi 
                    <i class="fas fa-angle-down text-sm ml-1"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ url('/about') }}">Tentang Kami</a>
                    <a class="dropdown-item" href="{{ url('/services') }}">Kebijakan</a>
                    <a class="dropdown-item" href="{{ url('/privacy') }}">Ketentuan</a>
                </div>
            </li>
            <li class="nav-item d-none d-lg-block">
                <a class="btn btn-sm" id="nav-button" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Sign In</a>
            </li> 
            <li class="nav-item d-none d-lg-block">
                <a class="btn btn-sm" id="nav-button" href="{{ route('register') }}"><i class="fas fa-terminal"></i> Sign Up</a>
            </li>
            <li class="nav-item d-block d-lg-none">
                <a class="nav-link" data-widget="pushmenu" href="#" id="show_sidebar"><i
                class="fas fa-bars"></i></a>
            </li>
        </ul>
    </div>
</nav>