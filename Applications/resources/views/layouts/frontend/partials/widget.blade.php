<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-5m+d1+5s-mf+hy"
     data-ad-client="ca-pub-9070824081326686"
     data-ad-slot="5334813540"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

<!-- Most Popular -->
<div class="card border-0 latest-posts" id="most-popular" style="margin-top: ">
    <div class="card-header border-bottom-0">
        <h5 class="card-title header-title">Most Popular Post</h3>
    </div>
    <div class="card-body">
        @foreach ($popular_posts as $post)
            <a href="{{ url('/post/'. $post->slug) }}" class="most-popular-link">
                <div class="media mb-3">
                    <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" alt="Tutorial HTML #4 Hello World" width="70">
                    <div class="media-body ml-2" style="margin-top: -3px">
                        <strong>{{ $post->title }}</strong>
                        <br>
                        <small class="d-inline-block text-indigo pr-1"><i class="far fa-calendar"></i> {{ $post->created_at->toFormattedDateString() }}</small>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
</div>

<!-- Categories -->
<div class="card m-1" id="categories">
    <div class="card-header border-bottom-0">
        <h5 class="card-title header-title">Categories</h5>
    </div>
    <div class="card-body pt-0">
        <ul class="nav nav-pills nav-sidebar flex-column">
            @foreach ($categories as $category)
                <li class="nav-item">
                    <a href="{{ url('/category/'. $category->slug) }}" class="nav-link bg-indigo">
                        <i class="nav-icon {{ $category->icon }}"></i>
                        <p>{{ $category->name }}</p>
                        <span class="badge badge-danger float-right">{{ $category->posts->count() }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<!-- Tags -->
<div class="card m-1" id="tags">
    <div class="card-header border-bottom-0">
        <h5 class="card-title header-title">Tags</h5>
    </div>
    <div class="card-body">
        @foreach ($tags as $tag)
            <a href="{{ url('/tag/'. $tag->slug) }}" class="btn btn-sm bg-indigo m-1">{{ $tag->name }}</a>
        @endforeach
    </div>
</div>

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-5m+d1+5s-mf+hy"
     data-ad-client="ca-pub-9070824081326686"
     data-ad-slot="5334813540"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>