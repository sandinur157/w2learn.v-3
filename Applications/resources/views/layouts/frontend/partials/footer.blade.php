<!-- Main Footer -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 footer-info">
                    <h3>W2LEARN</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, perspiciatis, eum culpa. <br>
                    Est expedita rerum eveniet iure totam sapiente rem accusantium, perspiciatis, eum culpa.</p>
                </div>
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('/about') }}">Tentang Kami</a></li>
                        <li><a href="{{ url('/services') }}">Kebijakan</a></li>
                        <li><a href="{{ url('/privacy') }}">Ketentuan</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Contact Us</h4>
                    <p>
                        W2learn Street <br>
                        Cirebon City, West Java<br>
                        Indonesia <br>
                        <strong>Phone:</strong> +62 813 2477 9934<br>
                        <strong>Email:</strong> supprt@w2learn.com<br>
                    </p>
                </div>
                <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Join Our Newsletter</h4>
                    <form action="{{ route('subscriber.store') }}" method="post">
                        <input type="hidden" name="_token">         
                        <input type="email" class="form-control form-control-sm" required name="email">
                        <button class="btn d-inline-block float-right rounded-0">Subscribe</button>
                    </form>

                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fab fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            © Copyright 2019 <strong><a href="{{ url('/') }}" class="text-white">W2learn</a></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="{{ url('/') }}">W2learn Team</a>
        </div>
    </div>
</footer>