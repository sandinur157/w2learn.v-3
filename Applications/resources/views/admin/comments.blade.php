@extends('layouts.master')

@section('title','Comments')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Comments</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <h5 class="card-title">All Comments</h5>
        @endslot

        @table
            @slot('thead')
                <th>Comments Info</th>
                <th>Post Info</th>
                <th>Action</th>
            @endslot

            @foreach($comments as $key => $comment)
            <tr>
                <td>
                    <div class="media">
                        <a href="#">
                            <img class="mr-3" src="{{ Storage::disk('public')->url('profile/'.$comment->user->image) }}" width="64" height="64">
                        </a>
                        <div class="media-body">
                            <h5 class="mt-0">{{ $comment->user->name }} <small class="text-muted">{{ $comment->created_at->diffForHumans() }}</small></h5>
                            <p>{{ $comment->comment }}</p>
                            <a class="btn btn-primary btn-sm float-right" target="_blank" href="{{ route('post.detail',$comment->post->slug.'#comments') }}"><i class="fas fa-reply"></i> </a>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="media">
                        <a target="_blank" href="{{ route('post.detail', $comment->post->slug) }}">
                            <img class="mr-3" src="{{ Storage::disk('public')->url('post/'.$comment->post->image) }}" width="64" height="64">
                        </a>
                        <div class="media-body">
                            <a target="_blank" href="{{ route('post.detail',$comment->post->slug) }}">
                                <h5 class="mt-0">{{ $comment->post->title }}</h5>
                            </a>
                            <p>By: <strong>{{ $comment->post->user->name }}</strong></p>
                        </div>
                    </div>
                </td>
                <td>
                    <form method="POST" action="{{ route('admin.comment.destroy', $comment->id) }}" >
                        @csrf
                        @method('DELETE')

                        <button type="button" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        @endtable

        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush