@extends('layouts.master')

@section('title','Tag')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('admin.tag.index') }}">Tag</a></li>
    <li class="breadcrumb-item">Add</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <h5 class="card-title">Add Tag</h5>
        @endslot
        
        <form action="{{ route('admin.tag.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Tag Name</label>
                <input type="text" id="name" class="form-control" name="name">
            </div>
        </form>

        @slot('footer')
            <a class="btn btn-danger" href="{{ route('admin.tag.index') }}">Back</a>
            <button type="submit" class="btn btn-primary" onclick="$('form').submit()">Save</button>
        @endslot
    @endcard
@endsection