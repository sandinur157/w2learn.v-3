@extends('layouts.master')

@section('title','Posts')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Posts</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <a href="{{ route('admin.post.create') }}" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add</a>
        @endslot
        
        @table

            @slot('thead')
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th><i class="fas fa-eye"></i></th>
                <th>Is Approved</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            @endslot
                                    
            @foreach($posts as $key => $post)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td title="{{ $post->title }}">{{ str_limit($post->title, '40') }}</td>
                    <td>{{ $post->user->name }}</td>
                    <td>{{ $post->view_count }}</td>
                    <td>
                        @if($post->is_approved == true)
                            <span class="badge bg-primary">Approved</span>
                        @else
                            <span class="badge bg-danger">Pending</span>
                        @endif
                    </td>
                    <td>
                        @if($post->status == true)
                            <span class="badge bg-primary">Published</span>
                        @else
                            <span class="badge bg-danger">Pending</span>
                        @endif
                    </td>
                    <td>{{ $post->created_at->toDateString() }}</td>
                    <td>{{ $post->updated_at->toDateString() }}</td>

                    <td class="text-center">
                        <form action="{{ route('admin.post.destroy', $post->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                             <a href="{{ route('admin.post.show',$post->id) }}" class="btn btn-outline-primary btn-sm"><i class="fas fa-eye"></i></a>
                            <a href="{{ route('admin.post.edit',$post->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endtable

        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush