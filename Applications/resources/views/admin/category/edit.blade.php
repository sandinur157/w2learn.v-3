@extends('layouts.master')

@section('title','Category')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('admin.category.index') }}">Category</a></li>
    <li class="breadcrumb-item">Add</li>
@endsection

@section('main-content')
    @card
        @slot('title')
            <h5 class="card-title">Add Category</h5>
        @endslot
        
        <form action="{{ route('admin.category.update',$category->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            
            <div class="form-group">
                <label for="name">Category Name</label>
                <input type="text" id="name" class="form-control" name="name" value="{{ $category->name }}">
            </div>

            <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile" name="image">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </form>

        @slot('footer')
            <a class="btn btn-danger" href="{{ route('admin.category.index') }}">Back</a>
            <button type="submit" class="btn btn-primary" onclick="$('form').submit()">Save</button>
        @endslot
    @endcard
@endsection