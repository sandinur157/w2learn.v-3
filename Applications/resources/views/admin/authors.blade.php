@extends('layouts.master')

@section('title','Authors')
@section('breadcrumb')
    @parent
    <li class="breadcrumb-item">Authors</li>
@endsection

@section('main-content')
 @card
        @slot('title')
            <h5 class="card-title">All Authors</h5>
        @endslot

        @table
            @slot('thead')
                <th>ID</th>
                <th>Name</th>
                <th>Posts</th>
                <th>Comments</th>
                <th>Favorite Posts</th>
                <th>Created At</th>
                <th>Action</th>
            @endslot
            @foreach($authors as $key => $author)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $author->name }}</td>
                    <td>{{ $author->posts_count }}</td>
                    <td>{{ $author->comments_count }}</td>
                    <td>{{ $author->favorite_posts_count }}</td>
                    <td>{{ $author->created_at->toDateString() }}</td>
                    <td class="text-center">
                        <form id="delete-form-{{ $author->id }}" action="{{ route('admin.author.destroy',$author->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" onclick="deleteAuthors({{ $author->id }})"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endtable

        @slot('footer')
            
        @endslot
    @endcard
@endsection

@push('scripts')
    <script>
        $('.table').DataTable({
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

        $('.card-footer').hide()
    </script>
@endpush