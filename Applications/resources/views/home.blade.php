@extends('layouts.frontend.master')

@section('title', 'Home')

@push('css')
    <style>
        .card-body h1,
        .card-body h2,
        .card-body h3:not(.header-title),
        .card-body h4,
        .card-body h5:not(.card-title),
        .card-body h6:not(.author)
        {
            font-size: 16px;
        }
    </style>
@endpush

@section('banner')
<section class="intro">
    <section class="home_banner_area">
        <div class="banner_inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="home_left_img">
                            <img src="{{ asset('/img') }}/home-left.png" alt="" class="img-fluid" style="width: 70%; margin: auto;">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_content">
                            <h2>
                                Pour Your Idea <br>
                                Into Coding <br>
                            </h2>
                            <p>Try Now</p>
                            <a href="#our-tutorials" class="btn btn-lg" id="started">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('main-content')
<div class="container-fluid">
    <div class="row justify-content-center" id="our-tutorials">
        <div class="col-lg-12">
            <h1 class="text-center header-title my-5">Our Tutorials</h1>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card m-2">
                <div class="card-body text-center">
                    <div class="img-over-home">
                        <img src="{{ asset('/img') }}/article.png" alt="" class="img-fluid" style="width: 100%">
                    </div>
                    <h3 class="my-3 header-title">Articles</h3>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="{{ url('/articles?page=2') }}" class="btn btn-indigo">Show Mores <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card m-2">
                <div class="card-body text-center">
                    <div class="img-over-home">
                        <img src="{{ asset('/img') }}/screencast.png" alt="" class="img-fluid" style="width: 100%">
                    </div>
                    <h3 class="my-3 header-title">Screencasts</h3>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <button disabled class="btn btn-indigo bg-secondary" style="cursor: not-allowed;">Coming Soon <i class="fas fa-times-circle"></i></button>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="card m-2">
                <div class="card-body text-center">
                    <div class="img-over-home">
                        <img src="{{ asset('/img') }}/course.png" alt="" class="img-fluid" style="width: 100%">
                    </div>
                    <h3 class="my-3 header-title">Courses</h3>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <button disabled class="btn btn-indigo bg-secondary" style="cursor: not-allowed;">Coming Soon <i class="fas fa-times-circle"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row mt-5">
        <div class="col-md-9">
            <h3 class="text-center header-title mb-5">Latest Articles</h3>
            <div class="row">
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- second ads -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="9828947544"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                <!-- Ads -->
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="fluid"
                     data-ad-layout-key="-5m+d1+5s-mf+hy"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="5334813540"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

                @foreach ($posts as $post)
                    <div class="col-lg-4">
                        <div class="card m-1">
                            <img src="{{ Storage::disk('public')->url('post/'. $post->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <small class="text-muted">
                                    <span class="date mt-1">{{ $post->created_at->toFormattedDateString() }}</span>
                                    <span class="float-right" style="margin-top: 4px;">
                                        @foreach($post->tags as $tag)
                                            <a class="text-indigo text-uppercase" href="{{ route('tag.posts', $tag->slug) }}">{{ $tag->name }}</a>,
                                        @endforeach
                                    </span>
                                </small>
                                <a href="{{ route('post.detail', $post->slug) }}" class="text-dark">
                                    <h5 class="card-title my-3">{{ $post->title }}</h5>
                                </a>
                                <p class="card-text card-content">
                                    {!! str_limit($post->body, 150) !!}
                                </p>

                                <div class="media">
                                    <img src="{{ Storage::disk('public')->url('profile/'.$post->user->image) }}" class="mr-3 rounded" alt="..." width="50">
                                    <div class="media-body">
                                        <h6 class="mt-0 ml-1 author">{{ $post->user->name }}</h6>
                                        <small class="d-inline-block text-muted pr-1"><i class="fas fa-clock"></i> {{ $post->created_at->diffForHumans() }}</small>
                                        <small class="d-inline-block text-muted border-left pl-2"><i class="fas fa-eye"></i> {{ $post->view_count }}</small> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <!-- Ads -->
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="fluid"
                     data-ad-layout-key="-5m+d1+5s-mf+hy"
                     data-ad-client="ca-pub-9070824081326686"
                     data-ad-slot="5334813540"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>

            </div>
            <div class="row">
                <div class="col-12">
                    <nav aria-label="Page navigation example">
                        {{ $posts->links('vendor.pagination.default') }}
                    </nav>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('layouts.frontend.partials.widget')
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('.content-wrapper').css('marginTop', '0')

        $(document).ready(function(){
            $("#started").on('click', function(e) {

                if (this.hash !== "") {
                    e.preventDefault();
                    let hash = this.hash;

                    $('html, body').animate({
                        scrollTop: $(hash).offset().top - 50
                    }, 900, function(){
                        window.location.hash = hash;
                    });
                }
            });
        })
    </script>
@endpush